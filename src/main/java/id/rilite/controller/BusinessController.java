package id.rilite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BusinessController {
    @GetMapping("/business")
    public String businessPage() {
        return "/business/businessList";
    }

    @GetMapping("/business/add")
    public String businessAddPage() {
        return "business/addBusiness";
    }

    @GetMapping("/business/edit")
    public String businessEditPage( @RequestParam(name = "businessId" ) Integer businessId, Model model )
    {
        model.addAttribute("businessId", businessId );
        return "business/editBusiness";
    }

    @GetMapping("/business/profile/{slug}")
    public String businessProfilePage(@PathVariable String slug) {
        return "/business/businessProfile";
    }

    @GetMapping("/business/teams")
    public String businessTeamsPage(@RequestParam(name = "businessId") Integer businessId, Model model)
    {
        model.addAttribute("businessId", businessId);
        return "/business/teamsList";
    }

    @GetMapping("/business/documents")
    public String businessDocumentsPage(@RequestParam(name="businessId") Integer businessId , Model model)
    {
        model.addAttribute("businessId" , businessId);
        return "/business/documentsList";
    }

    @GetMapping("business/reports")
    public String businessReportPage(@RequestParam(name="businessId") Integer businessId, Model model)
    {
        model.addAttribute("businessId" , businessId);
        return "/business/businessReport";
    }
}
