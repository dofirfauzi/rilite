package id.rilite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CollaborationController {
    @GetMapping("/business-collaboration")
    public String collaborationPage(){
        return "collaboration/index";
    }
    @GetMapping("/business-collaboration/{slug}")
    public String collaborationDetailPage(@PathVariable String slug){
        return "collaboration/detail";
    }
    @GetMapping("/business-collaboration/join")
    public  String collaborationJoinPage( @RequestParam(name = "collaborationId") Integer collaborationId , Model model)
    {
        model.addAttribute("collaborationId" , collaborationId);
        return "collaboration/formJoin";
    }
    @GetMapping("/collaboration")
    public String userCollaborationPage(){
        return "collaboration/collaborationList";
    }

    @GetMapping("/collaboration/add")
    public String userCollaborationAdd(){
        return "collaboration/addCollaboration";
    }

    @GetMapping("/collaboration/edit")
    public String userCollaborationEdit(@RequestParam(name= "collaborationId") Integer CollaborationId , Model model)
    { return "collaboration/editCollaboration"; }

    @GetMapping("collaboration/applied-user")
    public String appliedUserPage(@RequestParam(name="collaborationId") Integer CollaborationId , Model model)
    { return "collaboration/appliedUser"; }
}
