package id.rilite.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IncubationController {
    @GetMapping("/business-incubation")
    public String incubationPage(){
        return "incubation/index";
    }

    @GetMapping("/business-incubation-register")
    public String incubationRegistrationPage() {return "incubation/registration";}

    @GetMapping("/incubation")
    public String incubationListPage() { return "incubation/incubationList"; }

    @GetMapping("/incubation/participant")
    public String incubationParticipantPage( @RequestParam(name = "incubationId") String incubationId, Model model) {
        model.addAttribute("incubationId", incubationId );
        return "incubation/participant" ;
    }
}
