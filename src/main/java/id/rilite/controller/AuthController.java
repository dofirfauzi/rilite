package id.rilite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {
    @GetMapping("/signup")
    public String signUpPage() {
        return "auth/signup";
    }

    @GetMapping("/signin")
    public String signInPage() {
        return "auth/signin";
    }

    @GetMapping("/forgot-password")
    public String forgotPassword() {
        return "auth/forgotPassword";
    }

    @GetMapping("/change-password/{token}")
    public String changePassword() {
        return "auth/changePassword";
    }
}
