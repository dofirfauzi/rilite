package id.rilite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiliteApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiliteApplication.class, args);
	}

}
