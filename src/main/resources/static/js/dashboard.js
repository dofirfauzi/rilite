// toggle sidebar
const sidebarToggleButton = document.getElementById("sidebarToggler");
const sidebar = document.getElementById("sidebar");
const contentWrapper = document.getElementById("content-wrapper")
sidebarToggleButton.addEventListener("click", function () {
    sidebar.classList.toggle("toggle")
    contentWrapper.classList.contains("content-wrapper") ? contentWrapper.classList.remove("content-wrapper") : contentWrapper.classList.add("content-wrapper")
})

// sidebar-menu
const path = window.location.href.split('?') ;
const navLink = document.querySelectorAll("#sidebar .nav-ac-item .nav-ac-item-link")
navLink.forEach(function (e) {
    if (e.href === path[0]) {
        e.parentNode.classList.add('active')
    }
})

const subMenu = document.querySelectorAll("#sidebar .nav-ac-item .sub-menu .sub-menu-link")
subMenu.forEach(function (e) {
    if (e.href === path[0]) {
        bsCollapse = new bootstrap.Collapse(e.parentNode.parentNode.parentNode, {
            toggle: true
        });
        e.parentNode.classList.add('active');
        e.parentNode.parentNode.parentNode.parentNode.classList.add('active');
    }
})

//delete item function
function deleteItem(link, message){
    const confirmDelete = confirm(message);
    if (confirmDelete === true)
        window.location.href = link
}
