const path = window.location.href.split('?');
const navLink = document.querySelectorAll('.navbar .navbar-nav .nav-link');
navLink.forEach(function (e){
    if (e.href === path[0]) {
        e.classList.add('active');
    }
})

const navbar = document.getElementById('mainNavbar');
function addShadow () {
    if(window.scrollY > 10) {
        navbar.classList.add('navshadow');
    }else {
        navbar.classList.remove('navshadow');
    }
}
window.addEventListener('scroll', addShadow);

if(document.getElementsByClassName('mySwiper').length > 0) {
const swiper = new Swiper(".mySwiper", {
    pagination: {
        el: ".swiper-pagination",
        dynamicBullets: true,
        clickable: true
    },
});
}